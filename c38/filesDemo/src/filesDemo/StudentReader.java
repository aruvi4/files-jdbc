package filesDemo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class StudentReader {
	
	private static double median(List<Integer> numbers) {
		Collections.sort(numbers);
		int size = numbers.size();
		if (size % 2 == 1)
			return numbers.get(size / 2);
		return 0.5 * (numbers.get(size / 2 - 1) + numbers.get(size / 2));
	}

	
	public static void main(String[] args) {
		Path file = Paths.get("/home/aruvi/c38/demo");
		Path output = Paths.get("/home/aruvi/c38/output");
		try {
			BufferedReader br = Files.newBufferedReader(file);
			String line = br.readLine();
			String topperName = "";
			int topperScore = 0;
			List<Integer> marks = new ArrayList<>();
			while (line != null) {
				String[] components = line.split(", ");
				if (Integer.valueOf(components[1]) > topperScore) {
					topperName = components[0];
					topperScore = Integer.valueOf(components[1]);
				}
				marks.add(Integer.valueOf(components[1]));
				line = br.readLine();
			}
			BufferedWriter bw = Files.newBufferedWriter(output, StandardOpenOption.APPEND);
			bw.write(topperName + "," + topperScore + "\n");
			bw.write("median: " + median(marks));
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
