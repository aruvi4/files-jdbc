package jdbcdemo;

public class EmployeeDepartment {
	
	private int depId;
	private String depName;
	private double salary;
	public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}
	public String getDepName() {
		return depName;
	}
	public void setDepName(String depName) {
		this.depName = depName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "EmployeeDepartment [depId=" + depId + ", depName=" + depName + ", salary=" + salary + "]";
	}
	public EmployeeDepartment(int depId, String depName, double salary) {
		super();
		this.depId = depId;
		this.depName = depName;
		this.salary = salary;
	}
	
	
}
