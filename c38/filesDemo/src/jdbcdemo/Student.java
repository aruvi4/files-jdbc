package jdbcdemo;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Student {
	
	private String dept;
	private long rollNo;
	private String firstName;
	private String lastName;
	private String city;
	private Date dateOfBirth;
	
	public Student (ResultSet rs) throws SQLException {
		String combinedRoll = rs.getString("rollNo");
		dept = combinedRoll.substring(0, 3);
		this.rollNo = Long.valueOf(combinedRoll.substring(4));
		firstName = rs.getString("firstName");
		lastName = rs.getString("lastName");
		city = rs.getString("city");
		dateOfBirth = rs.getDate("dateOfBirth");
	}

	public String getDept() {
		return dept;
	}

	public long getRollNo() {
		return rollNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getCity() {
		return city;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	@Override
	public String toString() {
		return "Student [dept=" + dept + ", rollNo=" + rollNo + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", city=" + city + ", dateOfBirth=" + dateOfBirth + "]";
	}
	
	
}
