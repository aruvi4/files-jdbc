package jdbcdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDAO {
	
	/*
	 * CREATE a Product
	 * READ a Product given its id or name
	 * UPDATE a Product having a particular id with new details
	 * DELETE a Product with a given id
	 */
	
	private static final String SEARCH_BY_ID = "select * from products where id = ?";
	private static final String UPDATE_PRODUCT = "update product set name=?, price=?, category_id=? where id=?";
	private CategoryDAO categoryDAO;
	
	public ProductDAO() {
		categoryDAO = new CategoryDAO();
	}
	
	public Product searchById(int id) throws SQLException {
		Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "javauser", "password");
		PreparedStatement pstmt = cnx.prepareStatement(SEARCH_BY_ID);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		Product product = null;
		if (rs.next()) {
			Category category = categoryDAO.searchById(rs.getInt("category_id"));
			product = new Product(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"), 
					category);
		}
		cnx.close();
		return product;
	}
	
	
	public boolean update(Product product) throws SQLException{
		Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "javauser", "password");
		PreparedStatement pstmt = cnx.prepareStatement(UPDATE_PRODUCT);
		pstmt.setString(1, product.getName());
		pstmt.setDouble(2, product.getPrice());
		pstmt.setInt(3, product.getCategory().getId());
		pstmt.setInt(4, product.getId());
		int rowsUpdated = pstmt.executeUpdate();
		cnx.close();
		return rowsUpdated == 1;
	}
	
	public boolean create(Product product) throws SQLException {
		//your code here
	}
	
	public boolean delete(Product product) throws SQLException {
		//your code here
	}
}
