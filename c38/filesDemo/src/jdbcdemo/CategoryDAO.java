package jdbcdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryDAO {
	
	private static final String SEARCH_BY_ID = "select * from categories where id = ?";
	
	public Category searchById(int id) throws SQLException {
		Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "javauser", "password");
		PreparedStatement pstmt = cnx.prepareStatement(SEARCH_BY_ID);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		Category category = null;
		if (rs.next())
			category = new Category(rs.getInt("id"), rs.getString("name"));
		cnx.close();
		return category;
	}
}
