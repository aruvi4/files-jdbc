CREATE TABLE `mydb`.`categories` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `mydb`.`products` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `price` DECIMAL(5,2) NULL,
  `category_id` INT NULL,
  PRIMARY KEY (`id`),
    FOREIGN KEY (`category_id`)
    REFERENCES `mydb`.`CATEOGIRES` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `mydb`.`categories` values (1, 'Phones'), (2, 'Headphones');
INSERT INTO `mydb`.`products` values (1, 'iPhone', 99999.00, 1),
(2, 'airpods', 30000.00, 2),
(3, 'galaxy', 80000.00, 1),
(4, 'bose bluetooth', 35000.00, 2);
